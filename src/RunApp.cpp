#include "RunApp.h"
#include <sstream>

RunApp::RunApp(void)
{
}

RunApp::~RunApp(void)
{
}

void RunApp::createScene()
{
	// Set ambient light
	mSceneMgr->setAmbientLight(Ogre::ColourValue(1, 1, 1));
	//set sky
	mSceneMgr->setSkyDome(true, "Examples/CloudySky", 3, 4);

	// // Create a light
	// Ogre::Light* l = mSceneMgr->createLight("MainLight");
	// l->setPosition(20,80,50);

	//setup body and animation
	setupBody();

	mCamera->setPosition(0,25,25);
	mCamera->lookAt(0,18,0);

	Ogre::StringVector items;
    items.push_back("state");
	mLoseDiag = mTrayMgr->createParamsPanel(OgreBites::TL_NONE, "LoseDiag", 200, items);
	mLoseDiag->setParamValue(0, "game over");
	mLoseDiag->hide();
}

void RunApp::setupBody()
{
    mHero = new Character(mSceneMgr, mCamera);
    mc = new MapCtrl(mSceneMgr, mHero);
}

bool RunApp::frameRenderingQueued( const Ogre::FrameEvent& evt )
{
	mHero->update(evt.timeSinceLastFrame);
    if (!mc->refresh()){
        lose();
    }
	return Base::frameRenderingQueued(evt);
}

// OIS::KeyListener
bool RunApp::keyPressed( const OIS::KeyEvent &evt )
{
	using namespace OIS;
    bool ifAlive = true;
	switch(evt.key)
	{
		case KC_RIGHT:
            ifAlive = mc->rightTurnEvent();
            break;
		case KC_LEFT:
            ifAlive = mc->leftTurnEvent();
            break;
		case KC_X:
            mc->startRightMove();
            break;
		case KC_Z:
            mc->startLeftMove();
            break;
		default:
			break;
	}
    if (!ifAlive){
        lose();
    }
	mHero->injectKeyDown(evt);
    return Base::keyPressed(evt);
}

bool RunApp::keyReleased( const OIS::KeyEvent &evt )
{
	using namespace OIS;
	switch(evt.key)
	{
		case KC_X:
            mc->stopRightMove();
            break;
		case KC_Z:
            mc->stopLeftMove();
            break;
		default:
			break;
	}
	mHero->injectKeyUp(evt);
    return Base::keyReleased(evt);
}

void RunApp::lose()
{
	Ogre::LogManager::getSingletonPtr()->
		logMessage("*** lose 123123***");
	mTrayMgr->moveWidgetToTray(mLoseDiag, OgreBites::TL_CENTER, 0);
    mLoseDiag->show();
    mHero->die();
}
