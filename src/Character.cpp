#include "Character.h"
String animNames[] =
{"IdleBase", "IdleTop", "RunBase", "RunTop", "HandsClosed", "HandsRelaxed", "DrawSwords",
"SliceVertical", "SliceHorizontal", "Dance", "JumpStart", "JumpLoop", "JumpEnd"};

Character::Character(SceneManager* sm, Camera* camera)
{ 
	mCamera = camera;
	mNode = sm->getRootSceneNode()->createChildSceneNode();
	mCameraNode = mNode->createChildSceneNode();
	mHeroNode = mNode->createChildSceneNode();

	mHero = sm->createEntity("Hero", "sinbad.mesh");
	mHeroNode->attachObject(mHero);
	mSword1 = sm->createEntity("Sword1","Sword.mesh");
	mSword2 = sm->createEntity("Sword2","Sword.mesh");
	mHero->attachObjectToBone("Sheath.L",mSword1);
	mHero->attachObjectToBone("Sheath.R",mSword2);

	mSpeed = 2.0;

	mHeroNode->setDirection(0, 0, 1);
	// mHeroNode->setPosition(0, 25, 0);
	// mHeroNode->setScale(5,5,5);
	mHeroNode->setPosition(0, 11.5, 0);
	mHeroNode->setScale(2.5,2.5,2.5);

	mCameraNode->attachObject(mCamera);

	mHero->getSkeleton()->setBlendMode(ANIMBLEND_CUMULATIVE);

	for (int i = 0; i < NUM_ANIMS; i++)
	{
		mAnims[i] = mHero->getAnimationState(animNames[i]);
		mAnims[i]->setLoop(true);
	}
	mAnims[3]->setEnabled(true);
	mAnims[2]->setEnabled(true);

	mState = 1; //set alive

	mDirection[FORWARD] = Vector3(0, 0, -1);
	mDirection[LEFT] = Vector3(-1, 0, 0);
	mDirection[BACKWARD] = Vector3(0, 0, 1);
	mDirection[RIGHT] = Vector3(1, 0, 0);
	mDirId = FORWARD;

	//mAction = NORMAL;
	mActionTime = 0;
	//mMove = NORMAL;
	//mAlive = true;
}

Character::~Character()
{
}

void Character::injectKeyDown( const OIS::KeyEvent &evt )
{

	// using namespace OIS;
	// switch(evt.key)
	// {
	// 	case KC_Z:
	// 		mState = mState | MOVE_LEFT;
	// 		break;
	// 	case KC_X:
	// 		mState = mState | MOVE_RIGHT;
	// 		break;

	// 	default:
	// 		break;
	// }
}

void Character::injectKeyUp( const OIS::KeyEvent &evt )
{
	// using namespace OIS;
	// switch(evt.key)
	// {
	// 	case KC_Z:
	// 		mState = mState & ~MOVE_LEFT;
	// 		break;
	// 	case KC_X:
	// 		mState = mState & ~MOVE_RIGHT;
	// 		break;
	// 	default:
	// 		break;
	// }
}

void Character::injectMouseUp( const OIS::MouseEvent &evt )
{
}

void Character::injectMouseDown( const OIS::MouseEvent &evt )
{
  
}

void Character::injectMouseMove( const OIS::MouseEvent &evt )
{
}

void Character::update(Real dt)
{
	if(!(mState & ALIVE))
		return;
	mAnims[3]->addTime(dt);
	mAnims[2]->addTime(dt);

	Vector3 pos = mNode->getPosition();
	pos += mDirection[mDirId] * mSpeed;
	mNode->setPosition(pos);

	act();
}

void Character::setSpeed(Real speed)
{
	mSpeed = speed;
}

void Character::turnLeft()
{
	if(mActionTime)
		return;
	Ogre::LogManager::getSingletonPtr()->
		logMessage("*** turnLeft ***");
	if(mDirId == RIGHT)
	{
		mDirId = FORWARD;
	}
	else
	{
		mDirId++;
	}
	mActionTime = 10;
	mState = mState | TURN_LEFT;
	mState = mState & ~TURN_RIGHT;
}

void Character::turnRight()
{
	if(mActionTime)
		return;
	Ogre::LogManager::getSingletonPtr()->
		logMessage("*** turnRight ***");
	if(mDirId == FORWARD)
	{
		mDirId = RIGHT;
	}
	else
	{
		mDirId--;
	}
	mActionTime = 10;
	mState = mState | TURN_RIGHT;
	mState = mState & ~TURN_LEFT;
}

void Character::act()
{
	if(!mActionTime)
	{
		mState = mState & ~TURN_LEFT;
		mState = mState & ~TURN_LEFT;
	}
	else
	{
		mActionTime--;
		// Ogre::LogManager::getSingletonPtr()->
		// 	logMessage("*** act ***");
		if(mState & TURN_LEFT)
			mNode->yaw(Radian(3.14159265358/20));
		else if(mState & TURN_RIGHT)
			mNode->yaw(Radian(-3.14159265358/20));

	}
 
	if((mState & MOVE_LEFT) && (mState & MOVE_RIGHT))
	{
		return;
	}
	else if(mState & MOVE_LEFT)
	{
		// moveLeft);
	}else if(mState & MOVE_RIGHT)
	{
		// moveRight();
	}
}

Vector3 Character::getPosition()
{
	return mNode->getPosition();
}

void Character::moveLeft()
{
	Vector3 pos = mNode->getPosition();
	if(mDirId == RIGHT)
	{
		pos += mDirection[0];
	}
	else
	{
		pos += mDirection[mDirId+1];
	}
	mNode->setPosition(pos);
}


void Character::moveRight()
{
	Vector3 pos = mNode->getPosition();
	if(mDirId == FORWARD)
	{
		pos += mDirection[3];
	}
	else
	{
		pos += mDirection[mDirId-1];
	}
	mNode->setPosition(pos);
}

void Character::die()
{
	mState = mState & ~ALIVE;
}
