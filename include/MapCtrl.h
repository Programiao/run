#include "Base.h"
#include "MaterialPool.h"
#include "Character.h"

class MapCtrl{
 private:
    Ogre::SceneManager *mSceneMgr;
    Character *mHero;

    struct node{                /* 单向链表，用于保存地图上的地形 */
        Ogre::SceneNode *mapNode;
        MaterialPool *mat;
        int direction;
        long posx;
        long posz;
        node *next;
        bool turnFlag;
   };
    node *h, *t, *tt;           /* 链表的首 当前 尾指针  */
    int zoom;                   /* 放大倍数 */
    int side;                   /* 地形块边长 */
    int edge;
    int step;                   /* 2个地形块的间距 step=zoom*side */
    bool moveLeftState, moveRightState;
    std::map<std::string, MaterialPool*> mats; /* 资源列表 */
    void rel();                                /* 释放地形块函数 */
    int checkState();                        /* 检查当前格子状态 */
    bool gameOver(int);                      /* 死亡函数 */
    void mHeroMove(int);                     /* 左右移动 */
    void addmap();
    int addmapn;

 public:
	MapCtrl(Ogre::SceneManager*, Character*);
	~MapCtrl();
    void add(std::string);      /* 添加地形块函数 */
    bool refresh();             /* 每次刷新执行的函数 */
    bool leftTurnEvent();       /* 左方向 左转 */
    bool rightTurnEvent();      /* 右方向 右转 */
    void startLeftMove();       /* Z键按下开始左移*/
    void startRightMove();      /* X键按下开始右移*/
    void stopLeftMove();        /* Z键释放结束左移*/
    void stopRightMove();       /* X键释放结束右移*/
};
