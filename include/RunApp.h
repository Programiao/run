#include "Base.h"
#include "Character.h"
#include "MapCtrl.h"

class RunApp :public Base
{
private:
	Character* mHero;
    MapCtrl *mc;
    OgreBites::ParamsPanel* mLoseDiag; 

	void setupBody();
public:
	RunApp(void);
	~RunApp(void);
protected:
	virtual void createScene();
	virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);
	// OIS::KeyListener
	virtual bool keyPressed( const OIS::KeyEvent &arg );
	virtual bool keyReleased( const OIS::KeyEvent &arg );

	void lose();	//show lose ui
};
